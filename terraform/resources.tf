resource "digitalocean_droplet" "DEPLOYMENT" {
  image  = "${var.image_base}"
  name   = "DEPLOYMENT"
  region = "tor1"
  size   = "512mb"

  #   user_data = "${file("config/webuserdata.sh")}"

  ssh_keys = [
    "${var.ssh_fingerprint}",
  ]

  #   connection {
  #     user        = "root"
  #     type        = "ssh"
  #     private_key = "${file(var.pvt_key)}"
  #     timeout     = "2m"
  #   }

  # provisioner "remote-exec" {
  #     inline = [
  #         "export PATH=$PATH:/usr/bin",
  #         # install nginx
  #         "sudo apt-get update",
  #         "sudo apt-get -y install nginx"
  #     ]
  # }
}
