# Create a new domain
# resource "digitalocean_domain" "default" {
#   name       = "brentonholswich.com"
#   ip_address = "${digitalocean_droplet.DEPLOYMENT.ipv4_address}"
# }

# Add a record to the domain
resource "digitalocean_record" "DEPLOYMENT" {
  domain = "brentonholswich.com"
  type   = "A"
  name   = "DEPLOYMENT"
  value  = "${digitalocean_droplet.DEPLOYMENT.ipv4_address}"
}
