variable "do_token" {}
variable "pub_key" {}
variable "pvt_key" {}
variable "ssh_fingerprint" {}
variable "image_base" {}

provider "digitalocean" {
  token = "${var.do_token}"
}
