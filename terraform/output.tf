output "ip_address" {
  value = "${digitalocean_droplet.DEPLOYMENT.ipv4_address}"
}
