### Startup Instructions

- Run docker-compose locally
- perform backups from `scripts` and `scripts/docker_backup`
- goto `scripts/setup_scripts`
- change the variables that ask to be changed in .env-example
- execute `./run-setup.sh`

## Running Terraform and ansible

- You will need to fill out the terraform varfile in the `scripts/terraform` dir
- run the `init` script
- run the `plan` script
- make sure you have run the `mysql-setup.sh` (goto `scripts/mysql` to learn what is needed)
- run the backup scripts (`backup-scripts-dir.sh` and `docker_backup/backup.sh`) in the `scripts` dir
- back out and run the `start-new-server.sh 1` script (we pass an argument on the first run only)