#! /bin/bash

echo "Going to Terraform dir"
cd ./terraform

./terraform-apply.sh

if [ $# -eq 0 ]
  then
    echo "Save the ip address for ansible"
  else
    echo "First run through - Save to known hosts"
    ./terraform-output.sh 1
fi

echo "Going to Ansible dir"
cd ../Ansible

./setup.sh