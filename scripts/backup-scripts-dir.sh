#! /bin/bash

cd ../

# Do a backup of the scripts
tar --exclude='./scripts/terraform' --exclude='./scripts/start-new-server.sh' --exclude='./scripts/ansible' --exclude='./scripts/setup_scripts' -czf ./backups/scripts.tar.gz ./scripts

cd ./scripts