#! /bin/bash

source ../../.env

cd ../../ansible

sed -i -bak "s/DEPLOYMENT/${SITE_NAME}/g" hosts docker_wordpress.yml server_setup.yml docker_import.yml docker_mysql_import.yml docker_site_files_import.yml
sed -i -bak "s|SSH_PRIVATE_KEY|${SSH_PRIVATE_KEY}|g" hosts

# Clean up after sed
rm hosts-bak docker_wordpress.yml-bak rm server_setup.yml-bak