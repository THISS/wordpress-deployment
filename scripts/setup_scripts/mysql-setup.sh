#! /bin/bash

source ../../.env

cd ../mysql

sed -i -bak "s|DEPLOYMENT_SITE|${SITE_URL}|g" mysql-change-domain.sql

# Clean up after sed
rm mysql-change-domain.sql-bak