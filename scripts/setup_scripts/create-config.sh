#! /bin/bash

# Find out what the site name is by the name of the directory
cd ../../
siteName=$(pwd | sed -n "s|\(.*/\)\([A-Za-z_0-9][A-Za-z_\-\.0-9]*\)$|\2|p")
cd ./scripts/setup_scripts

#.env in the root dir will hold variables for the scripts running ansible
cp .env-example .env

sed -i -bak "s/DEPLOYMENT/${siteName}/g" .env

# clean up after sed
rm .env-bak

mv .env ../../
