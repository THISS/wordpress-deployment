#! /bin/bash

source ../../.env

cd ../../terraform

sed -i -bak "s/DEPLOYMENT/${SITE_NAME}/g" resources.tf domain.tf output.tf

# Clean up after sed
rm resources.tf-bak
rm domain.tf-bak
rm output.tf-bak