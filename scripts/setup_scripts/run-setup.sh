#! /bin/bash

echo "Create the config"
./create-config.sh

echo "Create the Directories"
./create-dirs.sh

echo "Create Docker-Compose script"
./docker-variable-setup.sh

echo "terraform variable set up"
./terraform-setup.sh

echo "ansible variable set up"
./ansible-setup.sh

echo "Setup Complete - make sure you edit the .env file in the root directory"