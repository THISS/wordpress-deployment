#! /bin/bash

source ../../.env

echo "Make a temporary copy of the docker-compose file"
cp docker-compose-example.yml docker-compose.yml
cp docker-compose.yml docker-compose.yml.tmpl.j2

echo "Transform the docker-compose file"
sed -i -bak "s/MYSQL_ROOT_PASSWORD:.*/MYSQL_ROOT_PASSWORD: ${DATABASE_ROOT_PASSWORD}/g" docker-compose.yml
sed -i -bak "s/MYSQL_DATABASE:.*/MYSQL_DATABASE: ${DATABASE_NAME}/g" docker-compose.yml
sed -i -bak "s/MYSQL_USER:.*/MYSQL_USER: ${DATABASE_USERNAME}/g" docker-compose.yml
sed -i -bak "s/MYSQL_PASSWORD:.*/MYSQL_PASSWORD: ${DATABASE_PASSWORD}/g" docker-compose.yml
sed -i -bak "s/WORDPRESS_DB_NAME:.*/WORDPRESS_DB_NAME: ${DATABASE_NAME}/g" docker-compose.yml
sed -i -bak "s/WORDPRESS_DB_USER:.*/WORDPRESS_DB_USER: ${DATABASE_USERNAME}/g" docker-compose.yml
sed -i -bak "s/WORDPRESS_DB_PASSWORD:.*/WORDPRESS_DB_PASSWORD: ${DATABASE_PASSWORD}/g" docker-compose.yml

echo "Transform the docker ansible template file"
sed -i -bak "s/MYSQL_ROOT_PASSWORD:.*/MYSQL_ROOT_PASSWORD: {{ lookup('env', 'DATABASE_ROOT_PASSWORD') }}/g" docker-compose.yml.tmpl.j2
sed -i -bak "s/MYSQL_DATABASE:.*/MYSQL_DATABASE: {{ lookup('env', 'DATABASE_NAME') }}/g" docker-compose.yml.tmpl.j2
sed -i -bak "s/MYSQL_USER:.*/MYSQL_USER: {{ lookup('env', 'DATABASE_USERNAME') }}/g" docker-compose.yml.tmpl.j2
sed -i -bak "s/MYSQL_PASSWORD:.*/MYSQL_PASSWORD: {{ lookup('env', 'DATABASE_PASSWORD') }}/g" docker-compose.yml.tmpl.j2
sed -i -bak "s/WORDPRESS_DB_NAME:.*/WORDPRESS_DB_NAME: {{ lookup('env', 'DATABASE_NAME') }}/g" docker-compose.yml.tmpl.j2
sed -i -bak "s/WORDPRESS_DB_USER:.*/WORDPRESS_DB_USER: {{ lookup('env', 'DATABASE_USERNAME') }}/g" docker-compose.yml.tmpl.j2
sed -i -bak "s/WORDPRESS_DB_PASSWORD:.*/WORDPRESS_DB_PASSWORD: {{ lookup('env', 'DATABASE_PASSWORD') }}/g" docker-compose.yml.tmpl.j2

# Clean up after sed
rm docker-compose.yml-bak docker-compose.yml.tmpl.j2-bak

echo "Append the phpmyadmin snippet on the docker-compose.yml"
cat docker-phpmyadmin-snippet.yml >> docker-compose.yml

echo "Move the docker-compose to the main docker dir for localhost development"
mv docker-compose.yml ../../docker
echo "Move the ansible version to the docker_run role template dir"
mv docker-compose.yml.tmpl.j2 ../../ansible/roles/docker_run/templates