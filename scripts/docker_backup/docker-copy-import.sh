#! /bin/bash

#########
# VARS
#########

echo "Changing working dir to docker for site"
cd ../../docker

echo "Setting Variables"
DB_CONTAINER=$(sudo docker-compose ps -q db)

echo "Changing working dir to main dir for site"
cd ../

echo "Copying Import Scripts to Container Volume"
sudo docker cp ./scripts/mysql/mysql-import.sh ${DB_CONTAINER}:/var/lib/mysql
sudo docker cp ./scripts/mysql/mysql-change-domain.sql ${DB_CONTAINER}:/var/lib/mysql
sudo docker cp ./backups/wordpress.backup.sql ${DB_CONTAINER}:/var/lib/mysql

echo "Running Import Script"
sudo docker exec $DB_CONTAINER /var/lib/mysql/mysql-import.sh

echo "Removing Import Scripts"
sudo docker exec ${DB_CONTAINER} /bin/rm /var/lib/mysql/mysql-import.sh
sudo docker exec ${DB_CONTAINER} /bin/rm /var/lib/mysql/mysql-change-domain.sql
sudo docker exec ${DB_CONTAINER} /bin/rm /var/lib/mysql/wordpress.backup.sql

echo "IMPORT COMPLETED"