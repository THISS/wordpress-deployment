#! /bin/bash

#########
# VARS
#########

echo "Changing working dir to docker for site"
cd ../../docker

echo "Setting Variables"
DB_CONTAINER=$(docker-compose ps -q db)

echo "Container to backup ${DB_CONTAINER}"

DATE_DIR=$(date +%Y-%m)
DAY_DIR=$(date +%d)
TIME_DIR=$(date +%H-%M)
FULL_BACKUP_DIR="./backups/${DATE_DIR}/${DAY_DIR}/${TIME_DIR}"

echo "Changing working dir to main dir for site"
cd ../

echo "Copying Backup Script to Container Volume"
docker cp ./scripts/mysql/mysql-backup.sh ${DB_CONTAINER}:/var/lib/mysql

echo "Running Backup Script"
docker exec $DB_CONTAINER /var/lib/mysql/mysql-backup.sh

echo "Copy backup to host"
docker cp ${DB_CONTAINER}:/var/lib/mysql/wordpress.backup.sql ./backups

echo "Removing Backup Script"
docker exec $DB_CONTAINER /bin/rm /var/lib/mysql/mysql-backup.sh
docker exec $DB_CONTAINER /bin/rm /var/lib/mysql/wordpress.backup.sql

echo "Run the Site Files backup script"
cd ./scripts/site_files
./backup-site-files.sh
cd ../

echo "Run the Scripts backup script"
./backup-scripts-dir.sh
cd ../

echo "Make the archive directory"
mkdir -p $FULL_BACKUP_DIR

echo "Copying DB Backup to an archive directory"
tar -czf $FULL_BACKUP_DIR/wordpress.backup.sql.tar.gz ./backups/wordpress.backup.sql

echo "Copying site files backup to an archive directory"
cp ./backups/site-files.backup.tar.gz $FULL_BACKUP_DIR