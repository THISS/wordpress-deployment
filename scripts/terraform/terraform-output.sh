#! /bin/bash
#export TF_LOG=1 # enable terraform logging
echo "Getting Ip Address from Terraform State"

cd ../../terraform

IP_ADDRESS=$(terraform output | sed -n 's/.* \([0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\)/\1/p')

echo "Setting ip address in ansible hosts file"
cd ../ansible

sed -i -bak "s/\(.*ansible_host=\)\([0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\)\(.*\)/\1${IP_ADDRESS}\3/g" hosts

if [ $# -eq 1 ]
  then
    echo "adding the key to ssh"
    ssh-keyscan -H $IP_ADDRESS >> ~/.ssh/known_hosts
  else
    echo "skip adding the ssh to known hosts"
fi