#! /bin/bash
#export TF_LOG=1 # enable terraform logging
cd ../../terraform

terraform plan -var-file=../scripts/terraform/terraform.tfvars
