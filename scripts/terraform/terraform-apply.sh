#! /bin/bash
#export TF_LOG=1 # enable terraform logging
cd ../../terraform

terraform apply -var-file=../scripts/terraform/terraform.tfvars
