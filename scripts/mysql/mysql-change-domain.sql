UPDATE wp_options SET option_value = replace(option_value, '//localhost', '//DEPLOYMENT_SITE') WHERE option_name = 'home' OR option_name = 'siteurl';

UPDATE wp_posts SET guid = replace(guid, '//localhost','//DEPLOYMENT_SITE');

UPDATE wp_posts SET post_content = replace(post_content, '//localhost', '//DEPLOYMENT_SITE');

UPDATE wp_postmeta SET meta_value = replace(meta_value,'//localhost','//DEPLOYMENT_SITE');