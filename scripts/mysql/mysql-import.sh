#! /bin/bash

########
# Importing DB
########
echo "In Container: changing to shared volume"
cd /var/lib/mysql

# echo "In Container: remove the database ${MYSQL_DATABASE}"
# mysql -u $MYSQL_USER -p$MYSQL_PASSWORD drop $MYSQL_DATABASE # Not command for dropping a database

echo "In Container: running import of db ${MYSQL_DATABASE}"
mysql -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE < wordpress.backup.sql

########
# Change Domain
########

echo "In Container: update the domain references for database ${MYSQL_DATABASE}"
mysql -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE < mysql-change-domain.sql