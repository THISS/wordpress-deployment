#! /bin/bash

echo "In Container: changing to shared volume"
cd /var/lib/mysql

echo "In Container: performing backup of ${MYSQL_DATABASE}"
mysqldump -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE > wordpress.backup.sql

# mysqldump --databases database_one database_two > two_databases.sql
# mysqldump --all-databases > all_databases.sql