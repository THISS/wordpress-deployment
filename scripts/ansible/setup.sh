#! /bin/bash

source ../../.env

cd ../../ansible

ansible-playbook -u root -i ./hosts ./server_setup.yml
ansible-playbook -u $NEWUSER -i ./hosts ./docker_wordpress.yml